import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(0.0, 4.0, num=30)
y = np.linspace(0.0, 4.0, num=30)
data = np.array(
    [
        [1,1],
        [3,1],
        [3,2],
        [2,2],
        [1,1],
    ]
)
x, y = data.T
plt.plot(x,y, 'b-')
plt.xlabel('x os')
plt.ylabel('y os')
plt.title('Primjer')
plt.show()
